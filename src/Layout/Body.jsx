import React, { Component } from "react";
import Banner from "./Body/Banner";
import Content from "./Body/Content";

export default class Body extends Component {
  render() {
    return (
      <div>
        <Banner />
        <section className="pt-4">
          <div className="container px-lg-5">
            <Content />
          </div>
        </section>
      </div>
    );
  }
}
